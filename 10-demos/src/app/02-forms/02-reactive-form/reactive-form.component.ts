import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'aw-form',
  templateUrl: './reactive-form.component.html'
})
export class ReactiveFormComponent {

  theForm: FormGroup;

  constructor(formBuilder: FormBuilder) {

    this.theForm = new FormGroup({
      name: new FormControl(),
      password: new FormControl()
    });

    // Add Validation
    // this.theForm = new FormGroup({
    //   name: new FormControl('Tyler', [Validators.required]),
    //   password: new FormControl('', [Validators.required, Validators.minLength(3)])
    // });

    // The FormBuilder is aa helper to create FormGroups/FromControls:
    // this.theForm = formBuilder.group({
    //     'name': ['Tyler', Validators.required],
    //     'password': ['', [Validators.required, Validators.minLength(3)]]
    // });
  }

  onSubmit() {
    console.log('submitted: ', this.theForm.value);
  }

}



